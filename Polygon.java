package geometry;

import java.util.ArrayList;

abstract class Polygon{
    protected int nSides; // number of sides (lati)
    protected ArrayList<Double> sides; // each item will contain the length value for a side of the polygon


    /*  [[ a che pro avere entrambi questi due costruttori??? ]] */

    public Polygon(int nSides){                 //here I don't know the measures for each side
        this.nSides = nSides;
        this.sides = new ArrayList<Double>(nSides);     // [[ cosa succede qui??? ]]
    }

    public Polygon(ArrayList<Double> sides){    //here I know the measures for each side
        this.nSides = sides.size();
        this.sides = sides;
    }

    public double computePerimeter(){   // [[metodo "non" astratto perché valido per tutti ]]
        double sum = 0.0;
        for(int i=0; i < nSides; i++)
            sum += sides.get(i);        // [[qui aggiungo il valore dei lati "ottenuti" dall'arrayList]]

        return sum;
    }

    public abstract double computeArea();
}