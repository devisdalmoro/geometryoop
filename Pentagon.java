package geometry;

import java.util.ArrayList;

public class Pentagon extends Polygon {

    protected double base;
    protected double height;

    public Pentagon(ArrayList<Double> sides, double base, double height){
        super(sides);
        nSides = 5;
        this.base = base;
        this.height = height;
    }

    //implement abstract method of super class
    public double computeArea(){
        return (base*height)*nSides/2;
    }
    
}