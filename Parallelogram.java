package geometry;

import java.util.ArrayList;

class Parallelogram extends Polygon{
    protected double base;
    protected double height;

    public Parallelogram(ArrayList<Double> sides, double base, double height){
        super(sides);//calling constructor public Polygon(ArrayList<double> sides)
        //nSides = 4;
        this.base = base;
        this.height = height;

        //ArrayList<Double> sides = new ArrayList<>();

    }

    //implement abstract method of super class
    public double computeArea(){
        return base*height;
    }

}